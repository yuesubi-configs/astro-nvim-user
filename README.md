# AstroNvim User Configuration

My custom user configuration forked from
[AstroNvim user configuration example](https://github.com/AstroNvim/user_example)

## 🛠️ Installation

#### Clone AstroNvim

```shell
git clone https://github.com/AstroNvim/AstroNvim ~/.config/nvim
```

#### Clone the repository

```shell
git clone https://gitlab.com/yuesubi-configs/astro-nvim-user ~/.config/nvim/lua/user
```

#### Start Neovim

```shell
nvim
```
